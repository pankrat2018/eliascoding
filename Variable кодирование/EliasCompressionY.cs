﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Variable_кодирование
{
    static class EliasCompressionY
    {
        public static Random rnd = new Random(500);

        public static List<uint> generatePostingList(int n, int range)
        {
            if ((n < 1) || (n > range) || (range < 1)) Console.WriteLine("должно быть больше 1");

            List<uint> postingList = new List<uint>(n);

            if ((n <= 10000000) && (n * 1.1 < range))
            {
                HashSet<uint> hs = new HashSet<uint>();
                while (hs.Count < n)
                {
                    uint docID = (uint)rnd.Next(1, range);

                    if (hs.Add(docID)) postingList.Add(docID);
                }
                postingList.Sort();
            }
            else
            {
                for (uint i = 1; i <= range; i++)
                {
                    if (rnd.Next(range - (int)i) < (n - postingList.Count))
                    {
                        postingList.Add(i);
                    }
                }
            }

            return postingList;
        }


        public static void EliasCompress(List<uint> postingList, byte[] compressedBuffer, ref int compressedBufferPointer2)
        {

            Stopwatch sw = Stopwatch.StartNew();

            uint lastDocID = 0;

            ulong buffer1 = 0;
            int bufferLength1 = 0;
            ulong buffer2 = 0;
            int bufferLength2 = 0;

            uint largestblockID = (uint)postingList[postingList.Count - 1];
            double averageDelta = (double)largestblockID / (double)postingList.Count;
            double averageDeltaLog = Math.Log(averageDelta, 2);
            int lowBitsLength = (int)Math.Floor(averageDeltaLog); if (lowBitsLength < 0) lowBitsLength = 0;
            ulong lowBitsMask = (((ulong)1 << lowBitsLength) - 1);

            int compressedBufferPointer1 = 0;

            compressedBufferPointer2 = lowBitsLength * postingList.Count / 8 + 6;

            compressedBuffer[compressedBufferPointer1++] = (byte)(postingList.Count & 255);
            compressedBuffer[compressedBufferPointer1++] = (byte)((postingList.Count >> 8) & 255);
            compressedBuffer[compressedBufferPointer1++] = (byte)((postingList.Count >> 16) & 255);
            compressedBuffer[compressedBufferPointer1++] = (byte)((postingList.Count >> 24) & 255);

            compressedBuffer[compressedBufferPointer1++] = (byte)lowBitsLength;

            foreach (uint docID in postingList)
            {
                uint docIdDelta = (docID - lastDocID - 1);


                buffer1 <<= lowBitsLength;
                buffer1 |= (docIdDelta & lowBitsMask);
                bufferLength1 += lowBitsLength;
                while (bufferLength1 > 7)
                {
                    bufferLength1 -= 8;
                    compressedBuffer[compressedBufferPointer1++] = (byte)(buffer1 >> bufferLength1);
                }

                uint unaryCodeLength = (uint)(docIdDelta >> lowBitsLength) + 1;
                buffer2 <<= (int)unaryCodeLength;

                buffer2 |= 1;
                bufferLength2 += (int)unaryCodeLength;

                while (bufferLength2 > 7)
                {
                    bufferLength2 -= 8;
                    compressedBuffer[compressedBufferPointer2++] = (byte)(buffer2 >> bufferLength2);
                }

                lastDocID = docID;
            }

            if (bufferLength1 > 0)
            {
                compressedBuffer[compressedBufferPointer1++] = (byte)(buffer1 << (8 - bufferLength1));
            }

            if (bufferLength2 > 0)
            {
                compressedBuffer[compressedBufferPointer2++] = (byte)(buffer2 << (8 - bufferLength2));
            }

            Console.WriteLine("\rШифровка:   " + sw.ElapsedMilliseconds.ToString("N0") + " ms  " + postingList.Count.ToString("N0") + " DocID  delta: " + averageDelta.ToString("N2") + "  low bits: " + lowBitsLength.ToString() + "   bits/DocID: " + ((double)compressedBufferPointer2 * (double)8 / (double)postingList.Count).ToString("N2") + " (" + (2 + averageDeltaLog).ToString("N2") + ")  незашифр: " + ((ulong)postingList.Count * 4).ToString("N0") + "  шифр: " + compressedBufferPointer2.ToString("N0") + "  ratio: " + ((double)postingList.Count * 4 / compressedBufferPointer2).ToString("N2"));
        }

        public static uint[,] decodingTableHighBits = new uint[256, 8];
        public static byte[] decodingTableDocIdNumber = new byte[256];
        public static byte[] decodingTableHighBitsCarryover = new byte[256];

        public static void eliasFanoCreateDecodingTable()
        {
            for (int i = 0; i < 256; i++)
            {
                byte zeroCount = 0;
                for (int j = 7; j >= 0; j--)
                {
                    if ((i & (1 << j)) > 0)
                    {

                        decodingTableHighBits[i, decodingTableDocIdNumber[i]] = zeroCount;


                        decodingTableDocIdNumber[i]++;
                        zeroCount = 0;
                    }
                    else
                    {
                        zeroCount++;
                    }
                }
                decodingTableHighBitsCarryover[i] = zeroCount;
            }
        }

        public static void EliasDecompress(byte[] compressedBuffer, int compressedBufferPointer, uint[] postingList, ref int resultPointer)
        {
            Stopwatch sw = Stopwatch.StartNew();



            int lowBitsPointer = 0;
            ulong lastDocID = 0;
            ulong docID = 0;

            int postingListCount = compressedBuffer[lowBitsPointer++];
            postingListCount |= (int)compressedBuffer[lowBitsPointer++] << 8;
            postingListCount |= (int)compressedBuffer[lowBitsPointer++] << 16;
            postingListCount |= (int)compressedBuffer[lowBitsPointer++] << 24;

            byte lowBitsLength = compressedBuffer[lowBitsPointer++];

            byte lowBitsCount = 0;
            byte lowBits = 0;

            byte cb = 1;
            for (int highBitsPointer = lowBitsLength * postingListCount / 8 + 6; highBitsPointer < compressedBufferPointer; highBitsPointer++)
            {
                docID += decodingTableHighBitsCarryover[cb];
                cb = compressedBuffer[highBitsPointer];

                byte docIdNumber = decodingTableDocIdNumber[cb];
                for (byte i = 0; i < docIdNumber; i++)
                {
                    docID <<= lowBitsCount;
                    docID |= lowBits & ((1u << lowBitsCount) - 1u);

                    while (lowBitsCount < lowBitsLength)
                    {
                        docID <<= 8;

                        lowBits = compressedBuffer[lowBitsPointer++];
                        docID |= lowBits;
                        lowBitsCount += 8;
                    }
                    lowBitsCount -= lowBitsLength;
                    docID >>= lowBitsCount;


                    docID += (decodingTableHighBits[cb, i] << lowBitsLength) + lastDocID + 1u;
                    postingList[resultPointer++] = (uint)docID; lastDocID = docID; docID = 0;
                }
            }
            Console.WriteLine("\rРасшифровка: " + sw.ElapsedMilliseconds.ToString("N0") + " ms  " + postingListCount.ToString("N0") + " DocID");
        }

       
    }
}
