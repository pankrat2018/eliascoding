﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Variable_кодирование
{
    class Program
    {
      /*  private static void YElias()
        {
            int i = 1;
            int multiplier = 2;
            int zero = 0;
            while (true)
            {
                if (multiplier == i)
                {
                    Console.WriteLine("Ещё? y/n");
                    if (Console.ReadLine() == "n")
                        break;

                    multiplier *= 2;
                    zero++;
                }

                string binaryCode = Convert.ToString(i, 2);
                for (int j = 0; j < zero; j++) 
                   binaryCode = binaryCode.Insert(0, "0");

                if(i == 1)
                    Console.WriteLine("Число 1 || Кодовое слово 1 || Длина кодового слова 1");

                else
                    Console.WriteLine("Число {0} || Кодовое слово {1} || Длина кодового слова {2}", i, binaryCode, binaryCode.Length);

                i++;
            }
         }*/


        private static void YElias()
        {
            int i = 1;
            int multiplier = 2;
            int zero = 0;
            while (true)
            {
                if (multiplier == i)
                {
                    Console.WriteLine("Ещё? y/n");
                    if (Console.ReadLine() == "n")
                        break;

                    multiplier *= 2;
                    zero++;
                }

                string binaryCode = Convert.ToString(i, 2);
                for (int j = 0; j < zero; j++)
                    binaryCode = binaryCode.Insert(0, "0");

                if (i == 1)
                    Console.WriteLine("Число 1 || Кодовое слово 1 || Длина кодового слова 1");

                else
                    Console.WriteLine("Число {0} || Кодовое слово {1} || Длина кодового слова {2}", i, binaryCode, binaryCode.Length);

                i++;
            }
        }

        private static void FixedVariable()
        {
            int i = 1;
            int multiplier = 2;
            int zero = 0;
            while (true)
            {
                if (multiplier == i)
                {
                    Console.WriteLine("Ещё? y/n");
                    if (Console.ReadLine() == "n")
                        break;

                    multiplier *= 2;
                    zero++;
                }

                string binaryCode = Convert.ToString(i, 2);
                for (int j = 0; j < zero; j++)
                    binaryCode = binaryCode.Insert(0, "0");

                if (i == 1)
                    Console.WriteLine("Число 1 || Кодовое слово 1 || Длина кодового слова 1");

                else
                    Console.WriteLine("Число {0} || Кодовое слово {1} || Длина кодового слова {2}", i, binaryCode, binaryCode.Length);

                i++;
            }
        }
        static void Main(string[] args)
        {
             var t = new DateTime(2011, 12, 31, 23, 59, 59, 999);

             var e = new EliasGammaBufferEncoder();
             e.Append(7000);
             e.Append(t.Ticks);
             e.Append(1);
             var buffer = e.GetByteArray();

             Console.WriteLine("Buffer size {0}", buffer.Length);

             // Decode values and check
             var d = new EliasGammaBufferDecoder(buffer);
             var v = d.ReadValue();


             for(int i = 0; i < buffer.Length; i++)
             {
                 Console.WriteLine(buffer[i]);
             }
            YElias();
            Console.ReadKey();
        }
    }
}
