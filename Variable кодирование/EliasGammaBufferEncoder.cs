﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Variable_кодирование
{
    /// <summary>
    /// Elias gamma coding encoding
    /// </summary>
    class EliasGammaBufferEncoder
    {
        readonly MemoryStream _ms;
        byte _c;
        int _p;

        public EliasGammaBufferEncoder()
        {
            _ms = new MemoryStream();
            _c = 0;
            _p = 0;
        }

        void WriteBit(bool bit)
        {
            if (bit) _c |= (byte)(1 << _p);
            _p += 1;
            if (_p < 8) return;
            _ms.WriteByte(_c);
            _c = 0;
            _p = 0;
        }

        public void Append(long value)
        {
            if (value < 1) throw new ArgumentException("нужно больше  1");
            if (_p < 0) throw new Exception("буффер закрыт");
            var t = new BitArray(8192);
            var l = 0;
            while (value > 1)
            {
                int len = 0;
                for (var temp = value; temp > 0; temp >>= 1)  //  1+floor(log2(num))
                    len++;
                for (int i = 0; i < len; i++)
                    t[l++] = ((value >> i) & 1) != 0;
                value = len - 1;
            }
            for (--l; l >= 0; --l) WriteBit(t[l]);
            WriteBit(false);
        }

        public byte[] GetByteArray()
        {
            if (_p != 0) _ms.WriteByte(_c);

            var r = _ms.ToArray();
            _p = -1;
            _ms.Close();

            return r;
        }
    }
}
